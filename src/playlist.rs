use std::fs;

pub struct Playlist
{
    base_dir : Option<String>,
    elements : Vec<Playable>, 
}

impl Playlist
{
    pub fn new() -> Playlist
    {
        Playlist
        {
            base_dir : None,
            elements : Vec::<Playable>::new(),
        }
    }

    pub fn parse( path : &str ) -> Playlist
    {
        let file_extension = crate::common::get_file_extension( &path );
        match file_extension
        {
            None => panic!("can't guess type of input file"),
            Some( extension ) => Playlist::parse_of_type( &path, &extension ),
        }
    }

    fn parse_of_type( path : &str, extension : &str ) -> Playlist
    {
        match extension
        {
            "m3u" => crate::m3u::parse( path ),
            _ => panic!("source file of unknown type: {}", extension),
        }
    }

    pub fn add_element(&mut self, element : Playable )
    {
        self.elements.push(element);
    }

    pub fn copy_elements( &self, to_path : &str )
    {
        for element in &self.elements
        {
            element.copy_to( to_path );
        }
    }
}
    
pub struct Playable
{
    pub path    : String,
}

impl Playable
{
    pub fn copy_to( &self, dest_path : &str )
    {
        let dest_path = dest_path.to_owned() + "/" + &crate::common::get_file_name_from_full_path(&self.path);
        std::fs::copy( &self.path, dest_path.clone() );
        print!("copyien {} to {}", self.path, dest_path );
    }
}
