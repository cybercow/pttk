//!Types and functions used across the project.

use crate::m3u;
use std::env;
use std::fs::File;
use std::fs;
extern crate fs_extra;

pub fn get_file_extension ( path : &str) -> Option<String> 
{
    let split = path.rsplit_once(".");
    match split
    {
        None => None,
        Some( strings ) => Option::Some( String::from(strings.1)),
    }
}

pub fn get_file_name_from_full_path( path : &str ) -> String
{
    let parts = path.rsplit_once("/");
    match parts
    {
        None => panic!("o no"),
        Some( strings ) =>
        {
            if strings.1.len() == 0
            {
                match strings.0.rsplit_once("/")
                {
                    None => panic!("o no"),
                    Some( other_strings ) =>
                    {
                        if other_strings.1.len() == 0
                        {
                            panic!("invalid path to copy to");
                        }else
                        {
                            other_strings.1.to_string()
                        }
                    },
                }
            }else
            {
                strings.1.to_string()
            }
        }
    }
}


pub struct ProgramState {
    programm_path   : String,
    programm_name   : String,
    source_path     : String,
    pub dest_path       : String,
    source_contents : Option<String>,
    root_dir        : Option<File>,
    dest_dir        : Option<File>,
    current_dir     : Option<File>,
}

impl ProgramState {
    pub fn get_source_dir_path(&self) -> String
    {
        let split_position = self.source_path.rfind('/');
        match split_position
        {
            None => String::from("./"),
            Some(position) => self.source_path[ .. position].to_string(),
        }
    }

    pub fn get() -> ProgramState {
        let args: Vec<String> = env::args().collect();
        match args.len(){
            1|2=> {
                    panic!("Too litle arguments, pttk needs a source file or directory and a destination file or directory");
                    },
            3 => ProgramState::parse_args( &args ),
            _ => panic!("Too much arguments there are no options implemented right now so please only give a source file or directory and a dest file or directory."),
        }
    }

    fn parse_args( args: &Vec<String> ) -> ProgramState{
        ProgramState {
            programm_path   : args[0].clone(),
            programm_name   : args[0].clone(),
            source_path     : args[1].clone(),
            dest_path       : args[2].clone(),
            source_contents : None,
            root_dir        : None,
            dest_dir        : None,
            current_dir     : None,
        }
    }

    pub fn open_source( &mut self ) -> crate::playlist::Playlist
    {
        if let Ok(meta) = std::fs::metadata(&self.source_path){
            if meta.is_dir(){
                panic!("'{}' is a directory, expacted a playlist file.", &self.source_path)
//                print!("bababumba");
//                fs_extra::dir::copy(&self.source_path, &self.dest_path, &fs_extra::dir::CopyOptions::new());
            }else if meta.is_file()
            {
                crate::playlist::Playlist::parse( &self.source_path )
            }else{
                panic!("{} is not a directory actually parsing files is not implemented yet.", &self.source_path);
            }

        }else{
            panic!("unable to open source file or directory:'{}'", &self.source_path);
        }
    }
}

pub fn get_contents_of( path : &str ) -> String
{
    if let Ok(meta) = std::fs::metadata(path)
    {
        if meta.is_dir()
        {
            panic!("'{}' is a directory awaiting a vile", path);
        }else if meta.is_file()
        {
            fs::read_to_string(path).expect("cant read source")
        }else
        {
            panic!("path points nether to file nor to directory.");
        }
        
    }else
    {
        panic!("unable to open source file or directory:'{}'", path);
    }
}
