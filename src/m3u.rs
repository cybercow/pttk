use crate::playlist;
use playlist::Playlist;
use crate::common;

pub fn parse( path:&str ) -> Playlist
{
   let contents = common::get_contents_of( path );
   print!("{}",contents);
   let mut playlist = Playlist::new();
   
   for line in contents.lines()
   {
       if (!line.starts_with('#')) & (line.len() > 0)
       {
            let element = playlist::Playable 
            {
                path : line.to_string(),
            };

            playlist.add_element( element );
        }
   }
    playlist
}
