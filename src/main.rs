mod common;
mod m3u;
mod playlist;
use common::ProgramState;

fn main() {
    let mut program_state = ProgramState::get();
    let playlist = program_state.open_source();
    playlist.copy_elements( &program_state.dest_path );
}
